require essioc
require rfcond

# General
epicsEnvSet("M", "DTL-050:SC-FSM-002")
epicsEnvSet("EVR", "DTL-050:RFS-EVR-101:")
epicsEnvSet("LLRF", "DTL-050:RFS-LLRF-101:")
epicsEnvSet("LLRFD", "DTL-050:RFS-DIG-101:")
epicsEnvSet("FIM", "DTL-050:RFS-FIM-101:")
epicsEnvSet("RFS", "DTL-050:RFS-")
#- VACX configuration for DTL
epicsEnvSet("VAC1","DTL-050:Vac-VGC-10000:PrsR CPP")
epicsEnvSet("VAC2","DTL-050:Vac-VGC-50000:PrsR CPP")
epicsEnvSet("VAC3","")
epicsEnvSet("VAC4","")

#- VACIX is different for DTL2 to DTL5
epicsEnvSet("VACI1","DTL-050:RFS-VacMon-110:Status-Ilck-RB.RVAL CPP")
epicsEnvSet("VACI2","DTL-050:RFS-VacMon-130:Status-Ilck-RB.RVAL CPP")
epicsEnvSet("VACI3","DTL-050:RFS-FIM-101:DI10-Ilck-RB.RVAL CPP")
epicsEnvSet("VACI4","DTL-050:RFS-FIM-101:DI12-Ilck-RB.RVAL CPP")

#Load standard module startup scripts
iocshLoad("$(essioc_DIR)/common_config.iocsh")


iocshLoad("$(rfcond_DIR)/rfcond.iocsh")

iocInit()
date                                                                                                                                           
